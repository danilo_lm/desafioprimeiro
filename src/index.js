import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  ScrollView,
} from 'react-native';

import 'config/ReactotronConfig';
import 'config/DevToolsConfig';

import Post from './components/Post';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#EE7777',

  },
  cabecalho: {
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: 20,
    textAlign: 'center',
    backgroundColor: '#FFFFFF',
    color: '#333333',
    padding: 20,
  },
});

export default class App extends Component {
  state = {
    posts: [
      {
        id: 0, titulo: 'Titulo 1', autor: 'Autor 1', descricao: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed faucibus sapien commodo velit efficitur condimentum. Duis pharetra at metus id mattis. Pellentesque faucibus nibh lacus, ac blandit mauris ornare in. Sed laoreet tellus nisi, sit amet venenatis massa sagittis ut. Phasellus ipsum mauris, pellentesque eget turpis eget, imperdiet maximus sem.',
      },
      {
        id: 1, titulo: 'Titulo 2', autor: 'Autor 2', descricao: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed faucibus sapien commodo velit efficitur condimentum. Duis pharetra at metus id mattis. Pellentesque faucibus nibh lacus, ac blandit mauris ornare in. Sed laoreet tellus nisi, sit amet venenatis massa sagittis ut. Phasellus ipsum mauris, pellentesque eget turpis eget, imperdiet maximus sem.',
      },
      {
        id: 2, titulo: 'Titulo 3', autor: 'Autor 3', descricao: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed faucibus sapien commodo velit efficitur condimentum. Duis pharetra at metus id mattis. Pellentesque faucibus nibh lacus, ac blandit mauris ornare in. Sed laoreet tellus nisi, sit amet venenatis massa sagittis ut. Phasellus ipsum mauris, pellentesque eget turpis eget, imperdiet maximus sem.',
      },
    ],
  }

  render() {
    return (
      <ScrollView style={styles.container}>
        <Text style={styles.cabecalho}>
          GoNative App
        </Text>
        { this.state.posts.map(post => (
          <Post key={post.id} titulo={post.titulo} autor={post.autor} descricao={post.descricao} />
        )) }
      </ScrollView>
    );
  }
}
