import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  box: {
    backgroundColor: '#FFFFFF',
    padding: 20,
    marginTop: 20,
    marginLeft: 20,
    marginRight: 20,
    justifyContent: 'center',
    borderRadius: 7,
  },

  titulo: {
    color: '#333333',
    fontWeight: 'bold',
  },

  autor: {
    color: '#999099',
  },

  separador: {
    marginBottom: 5,
    marginTop: 5,
    borderStyle: 'solid',
    borderColor: '#EEEEEE',
    borderWidth: 1,
  },

  descricao: {
    color: '#666666',
  },
});

export default class Post extends Component {
  render() {
    return (
      <View style={styles.box}>
        <Text style={styles.titulo}> { this.props.titulo } </Text>
        <Text style={styles.autor}> { this.props.autor } </Text>
        <View style={styles.separador}></View>
        <Text style={styles.descricao}> { this.props.descricao } </Text>
      </View>
    );
  }
}
